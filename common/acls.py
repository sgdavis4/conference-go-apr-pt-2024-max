from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    latlonresponse = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?q={city},{state},US&limit=1%units=imperial&appid={OPEN_WEATHER_API_KEY}"
    )
    latlon = json.loads(latlonresponse.content)
    if latlon["cod"] == "404":
        return {
            "temp": None,
            "description": None,
        }
    temp_in_K = latlon["main"]["temp"]
    desc = latlon["weather"][0]["description"]
    temp_in_F = round(((temp_in_K - 273.15) * 1.8 + 32), 2)
    try:
        return {
            "temp": temp_in_F,
            "description": desc,
        }
    except (KeyError, IndexError):
        return None
